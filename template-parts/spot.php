<div class="spot <?php echo isset($modifier_class)?$modifier_class:''; ?>">
    <div class="row">
        <div class="col-12 col-md-12 p-0 product-image">
            <div class="spot-img">
                <img src="../dist/img/icons/spot.png" alt="">
                <img class="__hover" src="../dist/img/icons/spot-hover.png" alt="">
            </div>     
        </div>

        <div class="col-12 col-md-12">
            <div class="spot-options">
                <div class="compra-rapida">
                    <a href="#">Compra Rápida</a>
                </div>
                <div class="color-selector">
                   <a href="#" class="color __white"></a>
                   <a href="#" class="color __blue"></a>
                   <a href="#" class="color __yellow"></a>
                   <a href="#" class="color __purple"></a>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-12 d-flex flex-column align-items-center product-description">
            <div class="spot-title">
                <h2 class="">Conjunto básico microfibra branco e azul</h2>
            </div>
            <div class="spot-aval">
                <span><img src="../dist/img/icons/aval-on.svg" alt=""></span>
                <span><img src="../dist/img/icons/aval-on.svg" alt=""></span>
                <span><img src="../dist/img/icons/aval-on.svg" alt=""></span>
                <span><img src="../dist/img/icons/aval-on.svg" alt=""></span>
                <span><img src="../dist/img/icons/aval-off.svg" alt=""></span>
            </div>
            <div class="spot-price">
                <span class="totalAvista"><span class="bold">R$ 64,90</span></span>
                <span class="precoParcela">
                    <span class="times">2x</span> <span class="valor-parcela">R$ 32,45</span>
                </span>
                <span class="totalParcela">Total parcelado: <span class="bold"> <br> R$ 1499,00</span></span>
            </div>
            <div class="spot-buttons">
                <a href="#" class="bt __bt-spot">Ver mais informações</a>
            </div>
        </div>
        <div class="fav">
            <a href="#">
                <svg class="icon-fav" xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 471.701 471.701">
                    <path fill="#6545db" d="M433.601 67.001c-24.7-24.7-57.4-38.2-92.3-38.2s-67.7 13.6-92.4 38.3l-12.9 12.9-13.1-13.1c-24.7-24.7-57.6-38.4-92.5-38.4-34.8 0-67.6 13.6-92.2 38.2-24.7 24.7-38.3 57.5-38.2 92.4 0 34.9 13.7 67.6 38.4 92.3l187.8 187.8c2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-3.9l188.2-187.5c24.7-24.7 38.3-57.5 38.3-92.4.1-34.9-13.4-67.7-38.1-92.4zm-19.2 165.7l-178.7 178-178.3-178.3c-19.6-19.6-30.4-45.6-30.4-73.3s10.7-53.7 30.3-73.2c19.5-19.5 45.5-30.3 73.1-30.3 27.7 0 53.8 10.8 73.4 30.4l22.6 22.6c5.3 5.3 13.8 5.3 19.1 0l22.4-22.4c19.6-19.6 45.7-30.4 73.3-30.4 27.6 0 53.6 10.8 73.2 30.3 19.6 19.6 30.3 45.6 30.3 73.3.1 27.7-10.7 53.7-30.3 73.3z"/>
                </svg>
            </a>
        </div>
        <div class="ticket __off"> <!-- Modificador para etiqueta de lançamento: __lancamento -->
            <span>10% <br> off</span>
        </div>

    </div>
</div> 
