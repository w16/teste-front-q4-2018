<!doctype html>
<html lang="pt-br">

<head>
	<meta charset="utf-8">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->

	<?php include('template-parts/vtex.premetatags.php') ?>
	<title>Teste Frontend CSS - Produto</title>

	<!-- <vtex:metaTags/> -->
	<?php include('template-parts/vtex.scripts.php') ?>

	<!-- <vtex:template id="commonStyles" /> -->
	<?php include('template-parts/commonStyles.php') ?>

</head>

<body class="page-product">

	<!--  SubTemplate: Header -->
	<!-- <vtex:template id="Header" /> -->
	<?php include('template-parts/header.php') ?>
	<!-- /SubTemplate: Header -->

	<section class="breadcrumbs">
		<div class="container">
			<!-- Começo do comando: <vtex.cmc:breadCrumb /> -->
			Breadcrumbs
	</section>

	<section class="details">
		<div class="container">
			<div class="row">
				<div class="col-lg-5">
					Galeria
				</div>
				<div class="col-lg-7">
					Informações e compra
				</div>
			</div>
		</div>
	</section>

	<?php include('template-parts/commonScripts.php'); ?>

</body>

</html>