<html>
<head>
	<style>
		ul {
			list-style: none;
			display: flex;
    }
    li {
      flex: 1;
    }
		a {
			height: 200px;
			display: flex;
			align-items: center;
			justify-content: center;
			text-transform: uppercase;
			text-decoration: none;
			color: black;
			background: white;
			transition: all 1s ease;
		}
		a:hover {
			background: black;
			color: white;
		}

	</style>
</head>
<body>
    <?php
    $json = file_get_contents('./package.json');
    $json = json_decode($json);
    ?>
    <h1>Projeto: <?php echo $json->name; ?></h1>


    <?php
    $dir    = '.';
    $files  = scandir($dir);?>

	<h2>Templates Disponíveis:</h2>

    <ul>
        <?php foreach($files as $f):?>
            <?php if(is_file($f) && strpos($f, 'php') && $f != 'index.php'): ?>
                <li><a href="/<?php echo $f; ?>"><?php echo str_replace('.php', '', $f); ?></a></li>
            <?php endif; ?>
        <?php endforeach;?>
    </ul>
</body>
</html>
