<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
    
    <?php include('template-parts/vtex.premetatags.php') ?>
    <title>Teste Frontend CSS - Home</title>
    
    <!-- <vtex:metaTags/> -->
    <?php include('template-parts/vtex.scripts.php') ?>
    
    <!-- <vtex:template id="commonStyles" /> -->
    <?php include('template-parts/commonStyles.php') ?>
    
</head>

<body class="page-home">
    
    <!--  SubTemplate: Header -->
    <!-- <vtex:template id="Header" /> -->
    <?php include('template-parts/header.php') ?>
    <!-- /SubTemplate: Header -->
    
    <div class="slideshow-wrapper">
        Slideshow
    </div>
    
    <section class="advantages">
        <div class="container">
            Régua de vantagens
        </div>
    </section>
    
    <div class="banner-wrapper">
        Banner
    </div>
    
    <!-- <vtex:template id="commonScripts" /> -->
    <?php include('template-parts/commonScripts.php'); ?>
    
</body>

</html>
