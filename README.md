# Bem-vindo ao teste de Front da W16!

Seu objetivo é deixar os templates de acordo com a referência localizada na pasta assets.

Lembrando que você avaliado pela organização, eficiência e legibilidade de código.

Faça um Fork privado no Bitbucket e dê acesso ao user developer@w16.com.br para que possamos ver sua entrega.

**Larguras de mancha:**
Desktop: 1200px
Mobile: 400px

### Importante!

 - Pense no responsivo!
 - Utilize Bootstrap v4
 - Utilize placeholder para imagens (https://placeholder.com/)
 - Utilize FontAwesome v4.7
 - Utilize SCSS
 - Utilize o mínimo necessário de JS
 - Utilize Slick Slide para Sliders (http://kenwheeler.github.io/slick/)

Desejado:
 - BEMCSS
 - Boas decisões para resolver problemas comuns de responsividade

### Para rodar o projeto
Instale via npm.
`npm start` para rodar o gulp.

Caso tenha dúvidas, entre em contato conosco talentos@w16.com.br

### Arquivos Referências
**Home**
assets\template-homepage.png
assets\template-homepage-mobile.png

**Menu**
assets\template-menu-aberto.png
assets\template-menu-aberto-mobile.png


