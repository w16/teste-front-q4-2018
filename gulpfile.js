'use strict';

const gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    postcss = require('gulp-postcss'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('autoprefixer'),
    cssnano = require('cssnano'),
    imagemin = require('gulp-imagemin'),
    browserSync = require('browser-sync').create(),
    php = require('gulp-connect-php'),
    fileSync = require('gulp-file-sync'),
    sassLint = require('gulp-sass-lint'),
    plumber = require('gulp-plumber');

const AUTOPREFIXER_BROWSERS = [
    'ie >= 10',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4.4',
    'bb >= 10'
];

const plugins = [
    autoprefixer({ browsers: AUTOPREFIXER_BROWSERS, flexbox: true }),
    cssnano()
];

// SCSS Dev
gulp.task('scss', function () {
    return gulp.src('./src/scss/**/*.scss')
        .pipe(plumber())
        .pipe(sass({includePaths: ['node_modules/']}))
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(sourcemaps.init())
        .pipe(postcss(plugins))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.stream());
});

// SCSS Minify
gulp.task('scss-build', function () {
    return gulp.src('./src/scss/**/*.scss')
        .pipe(sass({ includePaths: ['node_modules/'] }))
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(sourcemaps.init())
        .pipe(postcss(plugins))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.stream());
});

// JS Build
gulp.task('js', function () {
    return gulp.src([
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
        'node_modules/slick-carousel/slick/slick.min.js',
        './src/js/vendor/*/**.js',
        './src/js/**/*.js'
        ])
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(concat('app.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/js'))
        .pipe(browserSync.stream());
});

// SCSS Lint
gulp.task('lint', function () {
    return gulp.src('src/**/*.s+(a|c)ss')
        .pipe(sassLint({ files: {ignore: 'src/scss/_old*.scss' }}))
        .pipe(sassLint.format())
        .pipe(sassLint.failOnError())
});

// Image Build
gulp.task('img-compress', function () {
    gulp.src('./assets/img/**/*')
        .pipe(imagemin({
            progressive: true,
            interlaced: true
        }))
        .pipe(gulp.dest('dist/img'))
});

// Image Sync
gulp.task('img-sync', function () {
    fileSync('assets/img', 'dist/img', { recursive: true });
});

// Font Sync
gulp.task('font-sync', function () {
    fileSync('assets/fonts', 'dist/fonts', { recursive: true });
});


// PHP and Browser-sync
gulp.task('php', function () {
    php.server({ base: '.', port: 8001, keepalive: true });
});
gulp.task('watch', ['php'], function () {

    browserSync.init({
        proxy: '127.0.0.1:8001',
        port: 8000,
        open: true,
        notify: false
    });

    gulp.watch("src/scss/**/*.scss", ['scss']);
    gulp.watch("src/js/**/*.js", ['js']);
    gulp.watch("assets/**/*", ['img-sync', 'font-sync']).on('change', browserSync.reload);
    gulp.watch(["*.php", "template-parts/**/*.php"]).on('change', browserSync.reload);
});

gulp.task('default', ['scss', 'js', 'img-sync', 'font-sync']);

gulp.task('build', ['scss-build', 'js', 'img-compress', 'font-sync']);
